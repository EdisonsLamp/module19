#include <iostream>
#include <string>

class Animal
{

public:

     virtual void Voice()
    {
        std::cout << "String\n";
    }
};

class Dog : public Animal
{
public:

    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:

    void Voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Fox : public Animal
{
public:

    void Voice() override
    {
        std::cout << "Fusrodah!\n";
    }
};

int main()
{
    Animal* p[3] = { new Dog, new Cat, new Fox };
    for (Animal* element : p)
    {
        element->Voice();
    }

    return 0;
}
